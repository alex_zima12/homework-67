import React from "react";
import {useSelector, useDispatch} from "react-redux";

const Calculator = () => {

    const result = useSelector(state => state.value);
    const dispatch = useDispatch();
    const getResult = () => dispatch({type: "RESULT"})
    const clearValue = () => dispatch({type: "CLEAR"})
    const onClickHandler = (val) => dispatch({type: "ADD", symbol: val})
    const onClickHandlerOperation = (val) => dispatch({type: "ADDOPERATION", symbol: val})

    return (
        <div className="calculator">
            <div className="top">
                <button className='clear' onClick={clearValue}>Clear</button>
                <div className='screen'>{result}</div>
            </div>
            <div className='keys'>
                <button onClick={() => onClickHandler(7)} className="btn">7</button>
                <button onClick={() => onClickHandler(8)} className="btn">8</button>
                <button onClick={() => onClickHandler(9)} className="btn">9</button>
                <button onClick={() => onClickHandlerOperation("+")} className="btn">+</button>
                <button onClick={() => onClickHandler(4)} className="btn">4</button>
                <button onClick={() => onClickHandler(5)} className="btn">5</button>
                <button onClick={() => onClickHandler(6)} className="btn">6</button>
                <button onClick={() => onClickHandlerOperation("/")} className="btn">/</button>
                <button onClick={() => onClickHandler(1)} className="btn">1</button>
                <button onClick={() => onClickHandler(2)} className="btn">2</button>
                <button onClick={() => onClickHandler(3)} className="btn">3</button>
                <button onClick={() => onClickHandlerOperation("-")} className="btn">-</button>
                <button onClick={() => onClickHandler(0)} className="btn">0</button>
                <button onClick={() => onClickHandler(".")} className="btn">.</button>
                <button className='eval btn' onClick={getResult}>=</button>
                <button onClick={() => onClickHandlerOperation("*")} className="btn">*</button>
            </div>
        </div>
    );
};

export default Calculator;